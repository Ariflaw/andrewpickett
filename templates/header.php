<?php

    global $post;
    use Roots\Sage\Extras;

?>
<header class="main_header">
    <nav class="navbar navbar-expand-md" role="navigation">
      <div class="container">

            <div class="site_brand">
                <?= Extras\site_brand(); ?>
            </div>

            <!-- Brand and toggle get grouped for better mobile display -->
        	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
        		<!-- <span class="navbar-toggler-icon"></span> -->
                <span class="dashicons dashicons-menu"></span>
        	</button>

            <?php
                wp_nav_menu( array(
                    'theme_location'    => 'primary_navigation',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'bs-example-navbar-collapse-1',
                    'menu_class'        => 'nav navbar-nav ml-auto',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker()
        		) );
            ?>
        </div>
    </nav>

    <?php if ( is_page() && is_page_template() && ! is_home() && ! is_front_page() && ! is_page_template('full-width.php') ) { ?>
    <div class="page_hero" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full')); ?>')">
        <div class="container">
            <?php get_template_part('templates/page', 'header'); ?>
        </div>
    </div>
    <?php } ?>
</header>
