<footer class="main_footer">
    <div class="container">

        <div class="row sidebar_footer ">
            <div class="col-md-6 col-lg-4">
                <?php dynamic_sidebar('sidebar-footer-1'); ?>
            </div>
            <div class="col-md-6 col-lg-4">
                <?php dynamic_sidebar('sidebar-footer-2'); ?>
            </div>
            <div class="col-lg-4 ">
                <?php dynamic_sidebar('sidebar-footer-3'); ?>
            </div>
        </div>

        <nav class="nav_footer">
            <?php
            if (has_nav_menu('footer_nav')) :
                wp_nav_menu(['theme_location' => 'footer_nav', 'menu_class' => 'nav']);
            endif;
            ?>
        </nav>

        <div class="disclaimer">
            <p>The information on this website is for general information purposes only. Nothing on this site should be taken as legal advice for any individual case or situation. This information is not intended to create, and receipt or viewing does not constitute, an attorney-client relationship.</p>
        </div>
    </div>
</footer>


<div class="copyright">
    <div class="container">
        <span><?php esc_html_e( 'Copyright @ 2018 All Right Reserved.', 'andrew' ); ?></span>
    </div>
</div>
