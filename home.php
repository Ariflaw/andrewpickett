<?php
/**
 * Template Name: Homepage
 */

$bg_hero = "";
if ( get_field( 'bg_hero_home') ) {
 	$bg_hero = get_field( 'bg_hero_home' )['url'];;
}

?>
<?php if ( get_field( 'show_hero_home' ) == 1 ) { ?>
<div class="main_hero" style="background-image: url('<?php echo $bg_hero; ?>');">
    <div class="container">
        <div class="content">
        <?php if( get_field( 'title_hero_home' ) ) : ?>
            <h1><?php the_field( 'title_hero_home' ); ?></h1>
        <?php endif; ?>
        <?php if( get_field( 'desc_hero_home' ) ) : ?>
            <p><?php the_field( 'desc_hero_home' ); ?></p>
        <?php endif; ?>
        <?php if( get_field( 'btn_hero_home' ) ) : ?>
            <a href="<?php the_field( 'link_hero_home' ); ?>" class="btn_primary"><?php the_field( 'btn_hero_home' ); ?></a>
        <?php endif; ?>
        </div>
    </div>
</div>
<?php } ?>


<?php if ( get_field( 'show_desc_home' ) == 1 ) { ?>
<div class="description section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
            <?php if( get_field( 'title_desc_home' ) ) : ?>
                <h2 class="title_section"><?php the_field( 'title_desc_home' ); ?></h2>
                <span class="line"></span>
            <?php endif; ?>
            <?php if( get_field( 'desc_desc_home' ) ) : ?>
                <p class="mendium"><?php the_field( 'desc_desc_home' ); ?></p>
            <?php endif; ?>
            </div>
            <div class="col-md-7">
                <?php if( get_field( 'descl_desc_home' ) ) {
                    the_field( 'descl_desc_home' );
                } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if ( get_field( 'show_injury_home' ) == 1 ) { ?>
<div class="presentation section">
    <div class="container">
        <div class="content">
            <ul class="tab-content" id="present-tabContent">
                <?php if( have_rows('content_injury_home')): $i = 0; ?>
                    <?php while( has_sub_field('content_injury_home')):
                        $i++;
                        // vars
                		$heading = get_sub_field('heading_injury_home');
                		$bg_img = get_sub_field('bg_injury_home');
                		$info = get_sub_field('info_injury_home');
                        $desc = get_sub_field('desc_injury_home');

                    ?>
                        <li class="tab-pane fade" id="pre-quest-<?php echo $i;?>" role="tabpanel" aria-labelledby="pre-quest-tab">
                            <h3 class="title_content"><?php echo $heading; ?></h3>
                            <div class="tab_indicator">
                                <p class="step_number"></p>
                                <span class="line"></span>
                                <p><?php echo $info; ?></p>
                            </div>
                            <div class="pre_content" style="">
                                <img src="<?php echo $bg_img; ?>" alt="">
                                <div class="desc">
                                    <p><?php echo $desc; ?></p>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>

            <?php if( have_rows('content_injury_home') ): $i = 0; ?>
            <ul class="nav nav-pills justify-content-center" id="present-tab" role="tablist">

                <?php while(has_sub_field('content_injury_home')):
                    $i++;
                    // Var
                    $title_step = get_sub_field('titleStep_injury_home');
                ?>
                <li class="nav-item">
                    <a class="nav-link" id="pre-quest-tab" data-toggle="pill" href="#pre-quest-<?php echo $i; ?>" role="tab" aria-controls="pre-quest" aria-selected="true"><?php echo $title_step; ?></a>
                </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
        </div> <!-- .content -->
    </div>
</div>
<?php } ?>


<?php if ( get_field( 'show_exper_home' ) == 1 ) { ?>
<div class="experience section">
    <div class="container">
        <div class="row">
            <?php if ( get_field( 'img_exper_home') ) { ?>
            <div class="col-lg-5 box_profile">
                <div class="box_image">
                    <img class="profile" src="<?php the_field( 'img_exper_home' ); ?>" />
                </div>
                <div class="bg_profile"></div>
            </div>
            <?php } ?>
            <div class="col-lg-7">
                <div class="content">
                    <h3 class="title_section"><?php the_field( 'title_exper_home' ); ?></h3>
                    <?php the_field( 'desc_exper_home' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<?php if ( get_field( 'show_acc_home' ) == 1 ) { ?>
<div class="accident section">
    <div class="container">
        <h3 class="title_section"><?php the_field( 'titleSec_acc_home' ); ?></h3>

        <?php if( have_rows('content_acc_home') ): ?>
        <ul class="row content_list">
            <?php while( have_rows('content_acc_home') ): the_row();
                // vars
                $image = get_sub_field('imageCon_acc_home');
                $title = get_sub_field('titleCon_acc_home');
                $link = get_sub_field('linkCon_acc_home');

            ?>
            <li class="col-lg-3 col-md-6 content">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                <h5><?php echo $title; ?></h5>
                <a href="<?php echo $link; ?>"><?php esc_html_e( 'Learn More', 'andrew' ); ?></a>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>
<?php } ?>


<?php if ( get_field( 'show_cta_home' ) == 1 ) { ?>
<div class="call_action section">
    <div class="container">
        <div class="row">
            <h3><?php the_field('title_cta_home'); ?></h3>
            <?php if( get_field( 'btn_cta_home' ) ) { ?>
                <a href="<?php the_field( 'link_cta_home' ); ?>" class="btn_white ml-auto"><?php the_field('btn_cta_home'); ?></a>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>


<?php if ( get_field( 'show_symbol_home' ) == 1 ) {?>

<div class="collection section">
    <div class="container">
        <div class="row">
        <?php $images = get_field('gallery_symbol_home');
            if( $images ): ?>
            <?php foreach( $images as $image ): ?>
                <div class="col-md-4 col-sm-6">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="img_colection" />
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        </div><!-- .row -->
    </div><!-- .container -->
</div>
<?php } ?>


<?php if ( get_field( 'show_testi_home' ) == 1 ) { ?>
<div class="testimonial section">
    <div class="container">

        <h3 class="title_section"><?php the_field( 'titleSec_testi_home' ) ?></h3>

        <?php if( have_rows('content_testi_home') ): ?>
        <ul class="row">
            <?php while( have_rows('content_testi_home') ): the_row();
        		// vars
        		$name = get_sub_field('name_testi_home');
        		$time = get_sub_field('time_testi_home');
                $time = new DateTime($time);
                $star = get_sub_field( 'star_testi_home' );
                $content = get_sub_field('con_testi_home');

    		?>
            <li class="content_list col-lg-4 col-md-6 col-sm-12">
                <div class="info">
                    <span class="name"><?php echo $name; ?></span>
                    <time class="time"><?php echo $time->format('F j, Y'); ?></time>
                    <div class="star <?php echo $star; ?>">
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                    </div>
                    <div class="line"></div>
                </div>
                <div class="testi">
                    <p><?php echo $content; ?></p>
                </div>
            </li>

            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>

<?php } ?>


<?php if ( get_field( 'show_case_home' ) == 1 ) { ?>
<div id="contact_form" class="contact_form section">
    <div class="row">
        <div class="col-lg-5 image_screen" style="background-image:url('<?php the_field( 'img_bg_case_home' ); ?>');">
            <figcaption><?php the_field( 'desc_case_home' ); ?></figcaption>
        </div>

        <div class="col-lg-7 content_form">
            <?php
                $form = get_field('form_case_home');
                Ninja_Forms()->display($form);
            ?>
        </div>
    </div>
</div>
<?php } ?>
